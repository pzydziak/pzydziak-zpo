from django.shortcuts import render
from django.http import HttpResponse

from .models import Greeting
from .models import Part
from .models import CartEntry
from .models import BoughtEntry

# Create your views here.
def index(request):
    # return HttpResponse('Hello from Python!')
	
    parts = Part.objects.all()
	
    return render(request, "index.html", {"parts": parts})


def db(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, "db.html", {"greetings": greetings})
	
def db2(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, "db.html", {"greetings": greetings})
	
def cart(request):
	
    entries = CartEntry.objects.all()
    list = []
	
    for e in entries:
        correspPart = Part.objects.get(pk=e.partId)
        list.append(correspPart)
		
    
    return render(request, "cart.html", {"cartItems": list})

def cartPost(request, pk):
    part = Part.objects.get(pk=pk)
    cartEntry = CartEntry()
    cartEntry.partId = pk
    cartEntry.quantity = 1
    cartEntry.save()
	
    entries = CartEntry.objects.all()
    list = []
	
    for e in entries:
        correspPart = Part.objects.get(pk=e.partId)
        list.append(correspPart)
		
    
    return render(request, "cart.html", {"cartItems": list})
	
def boughtPost(request):
    entries = CartEntry.objects.all()
    for e in entries:
        bought = BoughtEntry()
        bought.partId = e.partId
        bought.save()
		
    entries.delete()
		
    boughts = BoughtEntry.objects.all()
    list = []
	
    for e in boughts:
        correspPart = Part.objects.get(pk=e.partId)
        list.append(correspPart)
		
    return render(request, "bought.html", {"boughtItems": list})
	
def bought(request):	
    boughts = BoughtEntry.objects.all()
    list = []
	
    for e in boughts:
        correspPart = Part.objects.get(pk=e.partId)
        list.append(correspPart)
	
    return render(request, "bought.html", {"boughtItems": list})
