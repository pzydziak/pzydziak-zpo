from django.db import models

# Create your models here.
class Greeting(models.Model):
    when = models.DateTimeField("date created", auto_now_add=True)

class Part(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField()
    image = models.FilePathField(path="/img")
    itemsLeft = models.IntegerField()
    createdON = models.DateTimeField(auto_now_add=True)
    price = models.DecimalField(max_digits=10, decimal_places=2)
	
class CartEntry(models.Model):
    partId = models.IntegerField()
    quantity = models.IntegerField()
	
class BoughtEntry(models.Model):
    partId = models.IntegerField()