from django.urls import path, include

from django.contrib import admin

admin.autodiscover()

import hello.views

# To add a new path, first import the app:
# import blog
#
# Then add the new path:
# path('blog/', blog.urls, name="blog")
#
# Learn more here: https://docs.djangoproject.com/en/2.1/topics/http/urls/

urlpatterns = [
    path("", hello.views.index, name="index"),
    path("db/", hello.views.db, name="db"),
	path("db2/", hello.views.db, name="db2"),
	path("cart/", hello.views.cart, name="cart"),
	path("cart/<int:pk>/", hello.views.cartPost, name="cartPost"),
	path("bought/post", hello.views.boughtPost, name="boughtPost"),
	path("bought", hello.views.bought, name="bought"),
    path("admin/", admin.site.urls),
]
